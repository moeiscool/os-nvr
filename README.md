<img src="https://gitlab.com/osnvr/os-nvr-assets/-/raw/master/screenshots/settings.png">

### OS-NVR is a lightweight extensible CCTV system.

- [Overview](#overview)
- [Features](#features)
- [Documentation](#documentation)
- [License](#license)

## Overview

##### Warning: this is a beta version, features are lacking. #1

"OS-NVR" is a temporary name. #2

Use [Issues]() for bug reports, feature requests and support.

## Features
- +80% test coverage
- Mobile-first user interface
- Bad motion detection addon

## Documentation
- [Installation](docs/1_Installation.md)
- [Configuration](docs/2_Configuration.md)
- [Development](docs/3_Development.md)
	
<br>

## Similar projects

- [Shinobi](https://gitlab.com/Shinobi/-Systems/Shinobi)
- [ZoneMinder](https://github.com/ZoneMinder/ZoneMinder)
- [Frigate](https://github.com/blakeblackshear/frigate)
- [Motion](https://github.com/Motion-Project/motion)[Eye](https://github.com/ccrisan/motioneye/)[OS](https://github.com/ccrisan/motioneyeos)

## License
All code is licensed under [GPL-2.0-only](LICENSE) 
